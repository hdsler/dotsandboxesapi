# DotsAndBoxesApi

Backend for Dots and Boxes game that supports multiple simultaneous game session with different grid sizes and persists status of the game session.
For using game api you will need to authenticate with one of 5 predefined users. In order to play a game user must create a new game and wait another 
player to connect to it. Then when it's users turn either /api/Game/AddLine or /api/Game/CrossOut endpoints must be used for interacting with game session. 
Each game update will return updated status of the game. At some point when one player gets so much point that another won't be able to get more even if he 
would crossout all cells that are left game ends. In this case game status would have a winner, otherwise if point counts for both players are the 
same it's a draw. When game ends users are not able to interact with game anymore, although still can access it's info.

Database: Sqlite for portability. Will be generated on first run of solution. 
There is initial existing game added between user1 and user2 that can be continued to be played.

There is a swagger for easier testing of the API. Swagger url: /swagger



======= Endpoints =======

/Account/Logout
logs user out

/Account/Login/Login
logs user in. There are 5 built in users:
login: user1; pass: qwerty
login: user2; pass: qwerty
login: user3; pass: qwerty
login: user4; pass: qwerty
login: user5; pass: qwerty


/api/Game/All
Gets all existing persisted games

/api/Game/GetById
Gets single game by game id

/api/Game/CreateGame
Creates a new game and connects to it. Field size might be from 3x3 to 255 cells

/api/Game/MissingPlayers
Gets all games that have single connected player and awaiting for another user

/api/Game/Connect
Connects to a specified game.

/api/Game/AddLine
Adds a line to game field. Line object consists of gameId and 2 coordinates (x1, y1, x2, y2). 
Origin of coordinates is at top left corder of the field from (0,0). Line size should always be 1 (e.g. (0,1;1,1)). (0,1;2,1) or (2,2;1,0) are invalid.
For example to add a line to the bottom of top left cell (0, 1; 1, 1) or (1,1; 0,1) coordinates must be used.

/api/Game/CrossOut
Crosses out a cell on the field. Cells origin of coordinates starts at top left corner of the field from (0,0). So coordinates of top left cell are (0, 0), to the right of it (1,0) etc.