﻿using System;
using Engine.Exceptions;
using Xunit;

namespace Engine.Tests
{
    public class TestEngine
    {
        private const string _player1 = "p1";
        private const string _player2 = "p2";
        
        [Fact]
        public void TestAddingSameLine()
        {
            IEngine engine = CreateEngineWithSimplestField();
            Line line = new Line(new Point(1, 0), new Point(1, 1));
            Assert.True(engine.IsAddingLineAllowed(line));
            engine.AddLine(line);

            Line sameLine = new Line(new Point(1, 0), new Point(1, 1));
            Assert.False(engine.IsAddingLineAllowed(sameLine));
            EngineException exception = Assert.Throws<EngineException>(() => engine.AddLine(sameLine));
            Assert.Equal($"Line {sameLine} already exists", exception.Message);
        }

        [Fact]
        public void TestCrossOut()
        {
            IEngine engine = CreateEngineWithSimplestField();
            Line line = new Line(1, 0, 1, 1);
            engine.AddLine(line);
            Point crossoutPoint = new Point(0, 0);
            EngineException exception =  Assert.Throws<EngineException>(() => engine.CrossOutCell(_player1, crossoutPoint)); 
            Assert.Equal($"{crossoutPoint} cell is not boxed", exception.Message);
            
            Line line2 = new Line(0, 1, 1, 1);
            engine.AddLine(line2);
            engine.CrossOutCell(_player1, crossoutPoint);
        }

        [Fact]
        private void TestFullGameOnSimplestField()
        {
            IEngine engine = CreateEngineWithSimplestField();

            AssertPlayersTurn(engine, _player1);
            engine.AddLine(new Line(3, 2, 2, 2));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(1, 1, 2, 1));
            
            AssertPlayersTurn(engine, _player1);
            engine.AddLine(new Line(2, 0, 2, 1));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(2, 1, 3, 1));
            
            AssertPlayersTurn(engine, _player2);
            Assert.False(engine.IsAddingLineAllowed(new Line(2, 1, 2, 2)));
            engine.CrossOutCell(_player2, new Point(2, 0));
            
            AssertPlayersTurn(engine, _player2);
            Assert.False(engine.IsCellCrossOutAllowed(new Point(1, 0)));
            engine.AddLine(new Line(1, 0, 1, 1));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(1, 0));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(2, 1, 2, 2));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(2, 1));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(2, 2, 1, 2));
            
            AssertPlayersTurn(engine, _player1);
            engine.AddLine(new Line(1, 1, 1, 2));
            
            AssertPlayersTurn(engine, _player1);
            engine.CrossOutCell(_player1, new Point(1, 1));
            
            AssertPlayersTurn(engine, _player1);
            engine.AddLine(new Line(0, 2, 1, 2));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(1, 2, 1, 3));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(0, 2));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(1, 1, 0, 1));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(0, 0));
            
            Assert.False(engine.IsPlayersTurn(_player1));
            Assert.False(engine.IsPlayersTurn(_player2));
            
            EngineException exception = Assert.Throws<EngineException>(() => engine.AddLine(new Line(2, 2, 2, 3)));
            Assert.Equal($"Game has already finished. Winner: p2", exception.Message);
        }

        [Fact]
        private void TestFullGameOnSimplestField2()
        {
            IEngine engine = CreateEngineWithSimplestField();

            AssertPlayersTurn(engine, _player1);
            engine.AddLine(new Line(1, 0, 1, 1));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(1, 1, 1, 2));
            
            AssertPlayersTurn(engine, _player1);
            engine.AddLine(new Line(1, 2, 1, 3));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(2, 0, 2, 1));
            
            AssertPlayersTurn(engine, _player1);
            engine.AddLine(new Line(2, 1, 2, 2));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(1, 1, 2, 1));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(1, 0));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(2, 1, 3, 1));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(2, 0));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(1, 2, 2, 2));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(1, 1));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(2, 2, 2, 3));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(1, 2));
            
            AssertPlayersTurn(engine, _player2);
            engine.AddLine(new Line(2, 2, 3, 2));
            
            AssertPlayersTurn(engine, _player2);
            engine.CrossOutCell(_player2, new Point(2, 1));
            
            EngineException exception = Assert.Throws<EngineException>(() => engine.AddLine(new Line(0, 1, 1, 1)));
            Assert.Equal($"Game has already finished. Winner: p2", exception.Message);
        }

        private void AssertPlayersTurn(IEngine engine, string player)
        {
            Assert.True(engine.IsPlayersTurn(player));
        }

        private IEngine CreateEngineWithSimplestField()
        {
            return Engine.CreateForNewGame(3, 3, _player1, _player2);
        }
    }
}