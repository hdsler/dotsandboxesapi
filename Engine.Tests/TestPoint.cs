﻿using System.Collections.Generic;
using Xunit;

namespace Engine.Tests
{
    public class TestPoint
    {
        [Fact]
        public void TestIEquatable()
        {
            Point point1 = new Point(1, 2);
            Point point2 = new Point(1, 2);
            Assert.True(point1.Equals(point2));
            
            Point point3 = new Point(0, 0);
            Assert.False(point1.Equals(point3));
        }

        [Fact]
        public void TestIEqualityComparer()
        {
            HashSet<Point> points = new HashSet<Point> {new Point(1, 2)};
            Assert.True(points.Contains(new Point(1, 2)));
            Assert.Equal(1, points.Count);
            points.Add(new Point(1, 2));
            Assert.Equal(1, points.Count);
            points.Add(new Point(2, 2));
            Assert.Equal(2, points.Count);
            Assert.False(points.Contains(new Point(0, 0)));
        }
    }
}