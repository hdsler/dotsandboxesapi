﻿using System.Collections.Generic;
using Xunit;

namespace Engine.Tests
{
    public class TestLine
    {
        [Fact]
        public void TestHash()
        {
            Assert.Equal(new Line(3, 2, 2, 2).GetHashCode(), new Line(2, 2, 3, 2).GetHashCode());
            Assert.Equal(new Line(0, 0, 0, 1).GetHashCode(), new Line(0, 1, 0, 0).GetHashCode());
            Assert.NotEqual(new Line(1, 0, 0, 0).GetHashCode(), new Line(0, 0, 0, 1).GetHashCode());
            Assert.NotEqual(new Line(1, 0, 0, 0).GetHashCode(), new Line(0, 1, 0, 0).GetHashCode());
        }
        
        [Fact]
        public void TestIEquatable()
        {
            Line line1 = new Line(new Point(0, 0), new Point(0, 1));
            Line line2 = new Line(new Point(0, 0), new Point(0, 1));
            Assert.True(line1.Equals(line2));
            Assert.True(line2.Equals(line1));
            
            Line line3 = new Line(new Point(1, 2), new Point(3, 4));
            Assert.False(line1.Equals(line3));
        }

        [Fact]
        public void TestEqualityBothWays()
        {
            Line line1 = new Line(new Point(2, 3), new Point(4, 5));
            Line line2 = new Line(new Point(4, 5), new Point(2, 3));
            Assert.True(line1.Equals(line2));
            Assert.True(line2.Equals(line1));
            
            Line line3 = new Line(new Point(3, 2), new Point(2, 2));
            Line line4 = new Line(new Point(2, 2), new Point(3, 2));
            Assert.True(line3.Equals(line4));
            Assert.True(line4.Equals(line3));
        }

        [Fact]
        public void TestIEqualityComparer()
        {
            HashSet<Line> lines = new HashSet<Line>
            {
                new Line(new Point(1, 2), new Point(3, 4))
            };
            Assert.True(lines.Contains(new Line(new Point(1, 2), new Point(3, 4))));
            Assert.False(lines.Contains(new Line(new Point(5, 4), new Point(3, 2))));
            lines.Add(new Line(new Point(5, 4), new Point(3, 2)));
            Assert.Equal(2, lines.Count);
            Assert.True(lines.Contains(new Line(new Point(5, 4), new Point(3, 2))));
        }
    }
}