﻿using System.Collections.Generic;

namespace Engine
{
    public class GameState
    {
        public List<PlayerData> Players;
        public PlayerData CurrentPlayer;
        public WinnerCheckResult WinnerCheckResult = null;
        
        public int CellsX;
        public int CellsY;
        
        public HashSet<Line> ExistingLines;

        public GameState()
        {
            
        }
        
        public GameState(int cellsX, int cellsY, string player1, string player2)
        {
            CellsX = cellsX;
            CellsY = cellsY;
            ExistingLines = new HashSet<Line>();
            Players = new List<PlayerData>
            {
                new PlayerData(player1),
                new PlayerData(player2)
            };
            CurrentPlayer = Players[0];
        }
    }
}