﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Engine.Exceptions;

namespace Engine
{
    public class Engine: IEngine
    {
        private readonly GameState _gameState;
        public string GetCurrentTurnPlayer() => _gameState.CurrentPlayer?.Player;
        
        public WinnerCheckResult GetWinnerCheckResult() => _gameState.WinnerCheckResult;
        
        public static IEngine CreateForNewGame(int cellsX, int cellsY, string player1, string player2)
        {
            ValidateFieldSize(cellsX, cellsY);
            return new Engine(cellsX, cellsY, player1, player2);
        }

        public static IEngine CreateForExistingGame(GameState gameState)
        {
            return new Engine(gameState);
        }
        
        private Engine(int cellsX, int cellsY, string player1, string player2)
        {
            _gameState = new GameState(cellsX, cellsY, player1, player2);
        }

        private Engine(GameState gameState)
        {
            _gameState = gameState;
        }

        public bool IsAddingLineAllowed(Line line)
        {
            return IsAddingLineAllowed(line, false);
        }
        
        public bool IsCellCrossOutAllowed(Point cell)
        {
            return IsCellCrossOutAllowed(cell, false);
        }
        
        public bool IsPlayersTurn(string player)
        {
            return _gameState.WinnerCheckResult == null && string.Equals(player, _gameState.CurrentPlayer.Player);
        }

        public void CrossOutCell(string player, Point cell)
        {
            IsCellCrossOutAllowed(cell, true);
            _gameState.CurrentPlayer.Crossouts.Add(cell);
            RunWinnerCheck();
        }
        
        public void AddLine(Line line)
        {
            ThrowIfLineNotValid(line);
            IsAddingLineAllowed(line, true);
            _gameState.ExistingLines.Add(line);
            if (!GetCellsToCrossOut().Any())
            {
                SwitchPlayer();
            }
            RunWinnerCheck();
        }

        [SuppressMessage("ReSharper", "ParameterOnlyUsedForPreconditionCheck.Local")]
        private static void ThrowIfLineNotValid(Line line)
        {
            if (Math.Abs(line.Point1.X - line.Point2.X) > 1 ||
                Math.Abs(line.Point1.Y - line.Point2.Y) > 1)
            {
                throw new EngineException("One line must not exceed size of 1");
            }

            if (line.Point1.X < 0 || line.Point2.Y < 0 ||
                line.Point1.Y >= 255 || line.Point2.Y >= 255)
            {
                throw new EngineException("Coordinates must be between 0 and 255");
            }
        }
        
        private void ValidateIfGameHasNoResultYet()
        {
            if (_gameState.WinnerCheckResult != null)
            {
                string winnerString = _gameState.WinnerCheckResult.Draw
                    ? "Game has already finished with draw."
                    : $"Game has already finished. Winner: {_gameState.WinnerCheckResult.WinnerPlayer}";
                throw new EngineException(winnerString);
            }
        }
        
        private bool IsAddingLineAllowed(Line line, bool throwException)
        {
            ValidateIfGameHasNoResultYet();
            if (GetExistingLines().Contains(line))
            {
                if (throwException)
                {
                    throw new EngineException($"Line {line} already exists");
                }

                return false;
            }
            
            IEnumerable<Point> cellsToCrossOut = GetCellsToCrossOut();
            if (cellsToCrossOut.Any())
            {
                if (throwException)
                {
                    throw new EngineException($"Can't add line {line}. Cell(s) must be crosed out first: {string.Join("; ", cellsToCrossOut)}");
                }

                return false;
            }

            return true;
        }

        private HashSet<Line> GetExistingLines()
        {
            HashSet<Line> lines = GenerateBorderLines(_gameState.CellsX, _gameState.CellsY).ToHashSet();
            lines.UnionWith(_gameState.ExistingLines);
            return lines;
        }

        private List<Point> GetCellsToCrossOut()
        {
            HashSet<Point> alreadyCrossedOutCells = new HashSet<Point>();
            foreach (PlayerData playerData in _gameState.Players)
            {
                alreadyCrossedOutCells.UnionWith(playerData.Crossouts);
            }

            // TODO: refactor this loop by checking only those cells that are close to the line that was added
            List<Point> points = new List<Point>();
            for (int x = 0; x < _gameState.CellsX; x++)
            {
                for (int y = 0; y < _gameState.CellsY; y++)
                {
                    Point point = new Point(x, y);
                    if (IsCellBoxed(point) && !alreadyCrossedOutCells.Contains(point))
                    {
                        points.Add(point);
                    }
                }
            }

            return points;
        }

        private bool IsCellCrossOutAllowed(Point cell, bool throwException)
        {
            ValidateIfGameHasNoResultYet();
            foreach (PlayerData playerData in _gameState.Players)
            {
                if (playerData.Crossouts.Contains(cell))
                {
                    if (throwException)
                    {
                        throw new EngineException($"{cell} cell already crossed out");
                    }
                    return false;
                }
            }

            if (!IsCellBoxed(cell))
            {
                if (throwException)
                {
                    throw new EngineException($"{cell} cell is not boxed");
                }

                return false;
            }

            return true;
        }

        private bool IsCellBoxed(Point cell)
        {
            List<Line> adjustingLines = new List<Line>
            {
                new Line(cell.X, cell.Y, cell.X + 1, cell.Y), // top
                new Line(cell.X, cell.Y + 1, cell.X + 1, cell.Y + 1), // bottom
                new Line(cell.X, cell.Y, cell.X, cell.Y + 1), // left
                new Line(cell.X + 1, cell.Y, cell.X + 1, cell.Y + 1) // right
            };
            HashSet<Line> existingLines = GetExistingLines();
            foreach (Line line in adjustingLines)
            {
                if (!existingLines.Contains(line)) return false;
            }

            return true;
        }

        private void RunWinnerCheck()
        {
            _gameState.WinnerCheckResult = CheckForWinner();
            if (_gameState.WinnerCheckResult != null)
            {
                _gameState.CurrentPlayer = null;
            }
        }

        private WinnerCheckResult CheckForWinner()
        {
            int alreadyCrossedOut = _gameState.Players.Select(p => p.Crossouts).Sum(v => v.Count);
            int emptyCells = _gameState.CellsX * _gameState.CellsY - alreadyCrossedOut;
            if (emptyCells == 0) // full field crossed out
            {
                if (_gameState.Players[0].Crossouts.Count > _gameState.Players[1].Crossouts.Count)
                {
                    return WinnerCheckResult.CreatePlayerWonResult(_gameState.Players[0].Player);
                }

                if (_gameState.Players[1].Crossouts.Count > _gameState.Players[0].Crossouts.Count)
                {
                    return WinnerCheckResult.CreatePlayerWonResult(_gameState.Players[1].Player);
                }
                return WinnerCheckResult.CreateDrawResult();
            }

            if (_gameState.Players[0].Crossouts.Count != _gameState.Players[1].Crossouts.Count)
            {
                PlayerData winningPlayer = _gameState.Players.OrderByDescending(p => p.Crossouts.Count).ElementAt(0);
                PlayerData losingPlayer = _gameState.Players.OrderByDescending(p => p.Crossouts.Count).ElementAt(1);
                if (losingPlayer.Crossouts.Count + emptyCells >= winningPlayer.Crossouts.Count)
                {
                    // winner player can still change. continue playing
                    return null;
                }
                return WinnerCheckResult.CreatePlayerWonResult(winningPlayer.Player);
            }

            // game continues
            return null;
        }

        private void SwitchPlayer()
        {
            _gameState.CurrentPlayer = _gameState.CurrentPlayer == _gameState.Players[0] ? _gameState.Players[1] : _gameState.Players[0];
        }

        private static void ValidateFieldSize(int cellsX, int cellsY)
        {
            if (cellsX < 3 || cellsY < 3 || cellsX > 255 || cellsY > 255)
            {
                throw new InvalidFieldSizeException(cellsX, cellsY);
            }
        }

        private static List<Line> GenerateBorderLines(int sizeX, int sizeY)
        {
            List<Line> lines = new List<Line>();
            for (int x = 0; x < sizeX; x++)
            {
                lines.Add(new Line(x, 0, x + 1, 0)); // top
                lines.Add(new Line(x, sizeY, x + 1, sizeY)); // bottom
            }

            for (int y = 0; y < sizeY; y++)
            {
                lines.Add(new Line(0, y, 0, y + 1)); // left
                lines.Add(new Line(sizeX, y, sizeX, y + 1)); // right
            }

            return lines;
        }
    }
}
