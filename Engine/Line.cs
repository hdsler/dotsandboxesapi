﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Engine
{
    public class Line: IEquatable<Line>, IEqualityComparer<Line>
    {
        public Point Point1 { get; }
        public Point Point2 { get; }

        public Line(Point point1, Point point2)
        {
            Point1 = point1;
            Point2 = point2;
        }

        public Line(int x1, int y1, int x2, int y2)
        {
            Point1 = new Point(x1, y1);
            Point2 = new Point(x2, y2);
        }

        public override string ToString()
        {
            return $"{Point1};{Point2}";
        }

        #region impl of IEquatable

        public bool Equals(Line other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Point1, other.Point1) && Equals(Point2, other.Point2) || 
                   Equals(Point1, other.Point2) && Equals(Point2, other.Point1);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Line) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                List<Point> orderedPoints =
                    new[] {Point1, Point2}
                        .OrderBy(p => p != null ? p.X : 0)
                        .ThenBy(p => p != null ? p.Y : 0)
                        .ToList();
                Point p1 = orderedPoints[0];
                Point p2 = orderedPoints[1];
                return ((p1 != null ? p1.GetHashCode() : 0)*397) 
                       + (p2 != null ? p2.GetHashCode() : 0);
            }
        }
        
        #endregion

        
        #region impl of IEqualityComparer

        public bool Equals(Line x, Line y)
        {
            return x.Equals(y);
        }

        public int GetHashCode(Line line)
        {
            return line.GetHashCode();
        }
        
        #endregion
    }
}