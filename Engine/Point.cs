﻿using System;
using System.Collections.Generic;

namespace Engine
{
    public class Point: IEqualityComparer<Point>, IEquatable<Point>
    {
        public int X { get; }
        public int Y { get; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        #region impl of IEqualityComparer 

        public bool Equals(Point first, Point second)
        {
            return first.Equals(second);
        }

        public int GetHashCode(Point point)
        {
            return point.GetHashCode();
        }
        
        #endregion

        #region impl of IEquatable 
        
        public bool Equals(Point other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Point) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }
        
        #endregion
    }
}