﻿using System;

namespace Engine.Exceptions
{
    public class EngineException: Exception
    {
        public EngineException(string message) : base(message)
        {
            
        }
    }
}