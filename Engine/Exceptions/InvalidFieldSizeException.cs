﻿namespace Engine.Exceptions
{
    public class InvalidFieldSizeException: EngineException
    {
        public InvalidFieldSizeException(int actualSizeX, int actualSizeY) : base($"Allowed field size 3x3 to 255x555 but got {actualSizeX}x{actualSizeY}")
        {
        }
    }
}