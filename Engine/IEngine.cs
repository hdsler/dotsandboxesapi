﻿namespace Engine
{
    public interface IEngine
    {
        bool IsAddingLineAllowed(Line line);
        void AddLine(Line line);

        bool IsCellCrossOutAllowed(Point cell);
        void CrossOutCell(string player, Point cell);

        bool IsPlayersTurn(string player);

        string GetCurrentTurnPlayer();
        WinnerCheckResult GetWinnerCheckResult();
    }
}