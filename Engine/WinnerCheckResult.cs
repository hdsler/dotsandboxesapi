﻿namespace Engine
{
    public class WinnerCheckResult
    {
        public bool Draw { get; }
        public string WinnerPlayer { get; }
        
        public static WinnerCheckResult CreateDrawResult()
        {
            return new WinnerCheckResult(true, null);
        }

        public static WinnerCheckResult CreatePlayerWonResult(string winnerName)
        {
            return new WinnerCheckResult(false, winnerName);
        }
        
        private WinnerCheckResult(bool draw, string winnerPlayer)
        {
            Draw = draw;
            WinnerPlayer = winnerPlayer;
        }
    } 
}