﻿using System.Collections.Generic;

namespace Engine
{
    public class PlayerData
    {
        public string Player { get; }
        public HashSet<Point> Crossouts { get; }
        
        public PlayerData(string playerName, HashSet<Point> points = null)
        {
            Player = playerName;
            Crossouts = points ?? new HashSet<Point>();
        }
    }
}