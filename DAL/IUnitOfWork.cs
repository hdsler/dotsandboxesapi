﻿using System.Threading.Tasks;
using DAL.Repositories;

namespace DAL
{
    public interface IUnitOfWork
    {
        IGameRepository Games { get; }

        ApplicationDbContext Context { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}