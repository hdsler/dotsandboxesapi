﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        //public string CurrentUserId { get; set; }
        public DbSet<ApplicationUserGameMap> ApplicationUserGameMaps { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Line> Lines { get; set; }
        public DbSet<Point> Points { get; set; }
        //public DbSet<LinePointMap> LinePointMaps { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Point>().HasOne(p => p.ApplicationUser).WithMany(u => u.Crossouts).HasForeignKey(c => c.CrossoutByApplicationUserId);
            builder.Entity<Point>().ToTable(nameof(Points));

            builder.Entity<Line>().HasMany(l => l.Points).WithOne(p => p.Line);
            builder.Entity<Line>().ToTable(nameof(Lines));

            builder.Entity<Game>().HasMany(g => g.ApplicationUserGameMaps).WithOne(m => m.Game);
            builder.Entity<Game>().HasMany(g => g.Crossouts).WithOne(m => m.Game);
            builder.Entity<Game>().HasMany(g => g.Lines).WithOne(m => m.Game);
            builder.Entity<Game>().HasOne(g => g.Winner).WithMany(u => u.WonGames).HasForeignKey(g => g.WinnerId);
            builder.Entity<Game>().ToTable(nameof(Games));

            builder.Entity<ApplicationUserGameMap>().HasOne(m => m.Game).WithMany(g => g.ApplicationUserGameMaps);
            builder.Entity<ApplicationUserGameMap>().HasOne(m => m.ApplicationUser).WithMany(g => g.ApplicationUserGameMaps);
            builder.Entity<ApplicationUserGameMap>().ToTable(nameof(ApplicationUserGameMaps));
        }
    }
}