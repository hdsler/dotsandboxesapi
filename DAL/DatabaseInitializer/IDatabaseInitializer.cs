﻿using System.Threading.Tasks;

namespace DAL.DatabaseInitializer
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }
}