﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace DAL.DatabaseInitializer
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public DatabaseInitializer(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task SeedAsync()
        {
            await _context.Database.MigrateAsync().ConfigureAwait(false);

            if (await _context.Users.AnyAsync())
            {
                return;
            }
            
            const string defaultPassword = "qwerty";
            await CreateUserAsync("user1", defaultPassword);
            await CreateUserAsync("user2", defaultPassword);
            await CreateUserAsync("user3", defaultPassword);
            await CreateUserAsync("user4", defaultPassword);
            await CreateUserAsync("user5", defaultPassword);

            ApplicationUser user1 = await _userManager.FindByNameAsync("user1");
            ApplicationUser user2 = await _userManager.FindByNameAsync("user2");

            CreateGame(user1, user2);
        }

        private async Task<Tuple<bool, string[]>> CreateUserAsync(string userName, string password)
        {
            ApplicationUser applicationUser = new ApplicationUser
            {
                UserName = userName
            };
            
            IdentityResult result = await _userManager.CreateAsync(applicationUser, password);
            if (!result.Succeeded)
            {
                return Tuple.Create(false, result.Errors.Select(e => e.Description).ToArray());
            }

            return Tuple.Create(true, new string[] { });
        }

        private void CreateGame(ApplicationUser user1, ApplicationUser user2)
        {
            Point line1Point1 = new Point {X = 1, Y = 0};
            Point line1Point2 = new Point {X = 1, Y = 1};
            
            Point line2Point1 = new Point {X = 1, Y = 1};
            Point line2Point2 = new Point {X = 1, Y = 2};
            
            Point line3Point1 = new Point {X = 2, Y = 0};
            Point line3Point2 = new Point {X = 2, Y = 1};
            
            Point line4Point1 = new Point {X = 2, Y = 1};
            Point line4Point2 = new Point {X = 2, Y = 2};
            
            Point line5Point1 = new Point {X = 1, Y = 2};
            Point line5Point2 = new Point {X = 1, Y = 3};

            Game game = new Game
            {
                ApplicationUserGameMaps = new List<ApplicationUserGameMap>
                {
                    new ApplicationUserGameMap
                    {
                        ApplicationUserId = user1.Id
                    },
                    new ApplicationUserGameMap
                    {
                        ApplicationUserId = user2.Id
                    }
                },
                NextTurnPlayerName = user1.UserName,
                FieldSizeX = 3,
                FieldSizeY = 3,
                Lines = new List<Line>
                {
                    new Line
                    {
                        Points = new List<Point>
                        {
                            line1Point1,
                            line1Point2
                        }
                    },
                    new Line
                    {
                        Points = new List<Point>
                        {
                            line2Point1,
                            line2Point2
                        }
                    },
                    new Line
                    {
                        Points = new List<Point>
                        {
                            line3Point1,
                            line3Point2
                        }
                    },
                    new Line
                    {
                        Points = new List<Point>
                        {
                            line4Point1,
                            line4Point2
                        }
                    },
                    new Line
                    {
                        Points = new List<Point>
                        {
                            line5Point1,
                            line5Point2
                        }
                    }
                },
                Crossouts = new List<Point>
                {
                }
            };
            _context.Add(game);
            _context.SaveChanges();
        }
    }
}