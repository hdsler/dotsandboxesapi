﻿using System.Collections.Generic;
using DAL.Entities;

namespace DAL.Repositories
{
    public interface IGameRepository: IRepository<Game>
    {
        IEnumerable<Game> GetGamesMissingPlayers();
    }
}