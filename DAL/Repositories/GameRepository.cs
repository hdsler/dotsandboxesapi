﻿using System.Collections.Generic;
using System.Linq;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace DAL.Repositories
{
    public class GameRepository: Repository<Game>, IGameRepository
    {
        public GameRepository(DbContext context) : base(context)
        {
        }

        public override IEnumerable<Game> GetAll()
        {
            return GetAllWithIncludes();
        }

        public IEnumerable<Game> GetGamesMissingPlayers()
        {
            return GetAll().Where(e => e.ApplicationUserGameMaps.Count < 2);
        }

        public override Game Get(int id)
        {
            return GetAllWithIncludes().FirstOrDefault(e => e.Id == id);
        }
        
        private IIncludableQueryable<Game, ApplicationUser> GetAllWithIncludes()
        {
            return Entities
                .Include(e => e.Lines).ThenInclude(l => l.Points)
                .Include(e => e.Crossouts).ThenInclude(c => c.ApplicationUser)
                .Include(e => e.ApplicationUserGameMaps).ThenInclude(m => m.ApplicationUser);
        }
    }
}