﻿namespace DAL.Entities
{
    public class Point: IHasId
    {
        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        
        public int? LineId { get; set; }
        public Line Line { get; set; }
        
        public int? GameId { get; set; }
        public Game Game { get; set; }
        
        public string CrossoutByApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}