﻿namespace DAL.Entities
{
    public enum EnumPointType
    {
        Undefined = 0,
        LinePart = 1,
        FieldSize = 2,
        Crossout = 3
    }
}