﻿namespace DAL.Entities
{
    public class ApplicationUserGameMap: IHasId
    {
        public int Id { get; set; }
        
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}