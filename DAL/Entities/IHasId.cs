﻿namespace DAL.Entities
{
    public interface IHasId
    {
        int Id { get; set; }
    }
}