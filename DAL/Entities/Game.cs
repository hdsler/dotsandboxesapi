﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Game: IHasId
    {
        public int Id { get; set; }
        
        public int FieldSizeX { get; set; }
        public int FieldSizeY { get; set; }
        
        public ICollection<ApplicationUserGameMap> ApplicationUserGameMaps { get; set; }
        public ICollection<Line> Lines { get; set; } 
        public ICollection<Point> Crossouts { get; set; } 
        
        public string NextTurnPlayerName { get; set; }
        public bool IsOver { get; set; }
        public string WinnerId { get; set; }
        public ApplicationUser Winner { get; set; }
    }
}

