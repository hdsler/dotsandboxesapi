﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Line: IHasId
    {
        public int Id { get; set; }
        public ICollection<Point> Points { get; set; }
        
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}