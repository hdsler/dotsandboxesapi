﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace DAL.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public ICollection<ApplicationUserGameMap> ApplicationUserGameMaps { get; set; }
        public ICollection<Point> Crossouts { get; set; }
        public ICollection<Game> WonGames { get; set; }
    }
}