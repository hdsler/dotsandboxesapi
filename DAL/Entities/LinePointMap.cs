﻿namespace DAL.Entities
{
    public class LinePointMap: IHasId
    {
        public int Id { get; set; }
        
        public int LineId { get; set; }
        public Line Line { get; set; }
        
        public int PointId { get; set; }
        public Point Point { get; set; }
    }
}