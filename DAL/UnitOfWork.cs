﻿using System.Threading.Tasks;
using DAL.Repositories;

namespace DAL
{
    public class UnitOfWork: IUnitOfWork
    {
        public ApplicationDbContext Context { get; }

        private IGameRepository _games;
        
        public UnitOfWork(ApplicationDbContext context)
        {
            Context = context;
        }
        
        public IGameRepository Games => _games ?? (_games = new GameRepository(Context));

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return Context.SaveChangesAsync();
        }
    }
}