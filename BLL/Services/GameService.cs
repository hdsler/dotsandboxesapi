﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DAL.Entities;
using Engine;
using Microsoft.AspNetCore.Identity;
using Point = DAL.Entities.Point;
using Line = DAL.Entities.Line;

namespace BLL.Services
{
    public class GameService: IGameService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        
        public GameService(IUnitOfWork unitOfWork, UserManager<ApplicationUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public IEnumerable<Game> GetGamesMissingPlayers()
        {
            return _unitOfWork.Games.GetGamesMissingPlayers().ToList();
        }

        public IEnumerable<Game> Getll()
        {
            return _unitOfWork.Games.GetAll().ToList();
        }

        public Game GetById(int gameId)
        {
            return _unitOfWork.Games.Get(gameId);
        }

        public async Task<Game> CreateNewGame(string username, int sizeX, int sizeY)
        {
            if (sizeX < 3 || sizeY < 3 || sizeX > 255 || sizeY > 255)
            {
                throw new InvalidOperationException("Invalid field size. Must be between 3x3 and 255x255");
            }
            
            ApplicationUser user = await _userManager.FindByNameAsync(username);
            Game newGame = new Game
            {
                FieldSizeX = sizeX,
                FieldSizeY = sizeY,
                IsOver = false,
                Winner = null,
                ApplicationUserGameMaps = new List<ApplicationUserGameMap>
                {
                    new ApplicationUserGameMap
                    {
                        ApplicationUserId = user.Id
                    }
                }
            };
            _unitOfWork.Games.Add(newGame);
            _unitOfWork.SaveChanges();
            return newGame;
        }

        public async Task<Game> ConnectToGame(string username, int gameId)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(username);
            Game game = GetGameThrowIfNotFound(gameId);
            
            if (game.ApplicationUserGameMaps.Count > 1 ||
                game.ApplicationUserGameMaps.Any(m => m.ApplicationUserId == user.Id))
            {
                throw new InvalidOperationException();
            }

            game.ApplicationUserGameMaps.Add(new ApplicationUserGameMap
            {
                ApplicationUserId = user.Id
            });
            game.NextTurnPlayerName = game.ApplicationUserGameMaps.ElementAt(new Random().Next(0, 2)).ApplicationUser.UserName;
            _unitOfWork.SaveChanges();
            return game;
        }

        public async Task<Game> AddLine(string username, int gameId, int x1, int y1, int x2, int y2)
        {
            Game game = GetGameThrowIfNotFound(gameId);
            ApplicationUser user = await _userManager.FindByNameAsync(username);
            ThrowIfNotAuthorizedForGame(user, game);
            
            IEngine engine = GetEngine(game, user);
            engine.AddLine(new Engine.Line(x1, y1, x2, y2));
            await UpdateGameState(game, engine);
            
            if (game.Lines == null)
            {
                game.Lines = new List<Line>();
            }
            game.Lines.Add(new Line { Points = new List<Point>
            {
                new Point { X = x1, Y = y1 }, 
                new Point { X = x2, Y = y2 }
            }});

            _unitOfWork.SaveChanges();
            return game;
        }

        public async Task<Game> AddCrossOut(string username, int gameId, int x, int y)
        {
            Game game = GetGameThrowIfNotFound(gameId);
            ApplicationUser user = await _userManager.FindByNameAsync(username);
            ThrowIfNotAuthorizedForGame(user, game);
            
            IEngine engine = GetEngine(game, user);
            engine.CrossOutCell(username, new Engine.Point(x, y));
            await UpdateGameState(game, engine);
            
            if (game.Crossouts == null)
            {
                game.Crossouts = new List<Point>();
            }
            game.Crossouts.Add(new Point{ X = x, Y = y, CrossoutByApplicationUserId = user.Id});
            _unitOfWork.SaveChanges();
            return game;
        }

        private static void ThrowIfNotAuthorizedForGame(ApplicationUser user, Game game)
        {
            if (game.ApplicationUserGameMaps.All(m => m.ApplicationUserId != user.Id))
            {
                if (game.ApplicationUserGameMaps.Count < 2)
                {
                    throw new InvalidOperationException("You are not part of this game. There are still slots available for joining");
                }
                throw new InvalidOperationException("You are not part of this game. No slots available");
            }
        }
        
        private async Task UpdateGameState(Game game, IEngine engine)
        {
            string player = engine.GetCurrentTurnPlayer();
            game.NextTurnPlayerName = player;
            WinnerCheckResult checkResult = engine.GetWinnerCheckResult();
            if (checkResult != null)
            {
                if (checkResult.WinnerPlayer != null)
                {
                    ApplicationUser winnerUser = await _userManager.FindByNameAsync(checkResult.WinnerPlayer);
                    game.WinnerId = winnerUser.Id;
                }

                game.IsOver = true;
            }
        }

        private static IEngine GetEngine(Game game, ApplicationUser user)
        {
            ThrowIfNotPlayersTurn(game, user);
            GameState gameState = CreateGameState(game);
            return Engine.Engine.CreateForExistingGame(gameState);
        }

        [SuppressMessage("ReSharper", "ParameterOnlyUsedForPreconditionCheck.Local")]
        private static void ThrowIfNotPlayersTurn(Game game, ApplicationUser user)
        {
            if (!string.Equals(game.NextTurnPlayerName, user.UserName, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new InvalidOperationException("It's not your turn");
            }
        }
        
        private static GameState CreateGameState(Game game)
        {
            List<PlayerData> players = new List<PlayerData>();
            foreach (ApplicationUserGameMap applicationUserGameMap in game.ApplicationUserGameMaps)
            {
                players.Add(new PlayerData(
                    applicationUserGameMap.ApplicationUser.UserName, 
                    game.Crossouts.Where(c => c.CrossoutByApplicationUserId == applicationUserGameMap.ApplicationUserId)
                        .Select(ToEnginePoint)
                        .ToHashSet()));
            }

            GameState gameState = new GameState
            {
                CellsX = game.FieldSizeX,
                CellsY = game.FieldSizeY,
                Players = players,
                CurrentPlayer = players.Single(p => p.Player == game.NextTurnPlayerName),
                ExistingLines = game.Lines.Select(ToEngineLine).ToHashSet(),
                WinnerCheckResult = null
            };
            return gameState;
        }

        private static Engine.Point ToEnginePoint(Point point)
        {
            return new Engine.Point(point.X, point.Y);
        }

        private static Engine.Line ToEngineLine(Line line)
        {
            return new Engine.Line(
                ToEnginePoint(line.Points.ElementAt(0)), 
                ToEnginePoint(line.Points.ElementAt(1)));
        }

        private Game GetGameThrowIfNotFound(int gameId)
        {
            Game game = _unitOfWork.Games.Get(gameId);
            if (game == null)
            {
                throw new InvalidOperationException("Game not found");
            }

            return game;
        }
    }
}