﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DAL.Entities;

namespace BLL.Services
{
    public interface IGameService
    {
        IEnumerable<Game> GetGamesMissingPlayers();
        IEnumerable<Game> Getll();
        Game GetById(int gameId);
        Task<Game> CreateNewGame(string username, int sizeX, int sizeY);
        Task<Game> ConnectToGame(string username, int gameId);
        Task<Game> AddLine(string username, int gameId, int x1, int y1, int x2, int y2);
        Task<Game> AddCrossOut(string username, int gameId, int x, int y);
    }
}