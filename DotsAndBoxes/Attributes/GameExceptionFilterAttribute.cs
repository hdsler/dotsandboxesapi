﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DotsAndBoxes.Attributes
{
    public class GameExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            ObjectResult result = new ObjectResult(context.Exception.Message);
            context.Result = result;
        }
    }
}