﻿using System.Linq;
using DAL.Entities;

namespace DotsAndBoxes.Dtos
{
    public static class MapperExtentions
    {
        public static GameDto ToDto(this Game game)
        {
            return new GameDto
            {
                Id = game.Id,
                FieldSizeX = game.FieldSizeX,
                FieldSizeY = game.FieldSizeY,
                Crossouts = game.Crossouts?.Select(c => c.ToCrossOutDto()).ToList(),
                Lines = game.Lines?.Select(l => l.ToDto()).ToList(),
                Players = game.ApplicationUserGameMaps.Select(u => u.ApplicationUser.ToDto()).ToList(),
                NextPlayerTurn = game.NextTurnPlayerName,
                Winner = game.Winner?.UserName,
                IsOver = game.IsOver
            };
        }

        public static LineDto ToDto(this Line line)
        {
            return new LineDto
            {
                Points = line.Points.Select(p => p.ToPointDto()).ToList()
            };
        }

        public static PlayerDto ToDto(this ApplicationUser appUser)
        {
            return new PlayerDto
            {
                Id = appUser.Id,
                UserName = appUser.UserName
            };
        }
        
        public static PointDto ToPointDto(this Point point)
        {
            return new PointDto
            {
                X = point.X,
                Y = point.Y
            };
        }

        public static CrossOutDto ToCrossOutDto(this Point point)
        {
            return new CrossOutDto
            {
                X = point.X,
                Y = point.Y,
                UserName = point.ApplicationUser.UserName
            };
        }
    }
}