﻿namespace DotsAndBoxes.Dtos
{
    public class CrossOutDto: PointDto
    {
        public string UserName { get; set; }
    }
}