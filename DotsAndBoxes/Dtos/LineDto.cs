﻿using System.Collections.Generic;

namespace DotsAndBoxes.Dtos
{
    public class LineDto {
        public List<PointDto> Points { get; set; }
    }
}