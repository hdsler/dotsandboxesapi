﻿namespace DotsAndBoxes.Dtos
{
    public class CrossoutCreationDto
    {
        public int GameId { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}