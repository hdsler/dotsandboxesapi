﻿namespace DotsAndBoxes.Dtos
{
    public class LineCreationDto
    {
        public int GameId { get; set; }
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }
    }
}