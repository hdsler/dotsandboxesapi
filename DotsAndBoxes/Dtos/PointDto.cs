﻿namespace DotsAndBoxes.Dtos
{
    public class PointDto
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}