﻿using System.Collections.Generic;
using System.Linq;
using DAL.Entities;

namespace DotsAndBoxes.Dtos
{
    public class GameDto
    {
        public int Id { get; set; }
        
        public int FieldSizeX { get; set; }
        public int FieldSizeY { get; set; }
        
        public List<PlayerDto> Players { get; set; }
        public List<LineDto> Lines { get; set; }
        public List<CrossOutDto> Crossouts { get; set; } 
        public string NextPlayerTurn { get; set; }
        public string Winner { get; set; }
        public bool IsOver { get; set; }
    }
}
