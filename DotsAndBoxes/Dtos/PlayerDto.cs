﻿namespace DotsAndBoxes.Dtos
{
    public class PlayerDto {
        public string Id { get; set; }
        public string UserName { get; set; }
    }
}