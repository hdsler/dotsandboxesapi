﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DotsAndBoxes.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace DotsAndBoxes.Controllers
{
    public interface IGameController
    {
        [HttpGet("All")]
        [Produces(typeof(IEnumerable<GameDto>))]
        IActionResult Get();
        
        [HttpGet("GetById")]
        [Produces(typeof(GameDto))]
        IActionResult Get(int gameId);

        [HttpPost("Create")]
        [Produces(typeof(GameDto))]
        Task<IActionResult> Post(int sizeX, int sizeY);

        [HttpGet("MissingPlayers")]
        [Produces(typeof(IEnumerable<GameDto>))]
        IActionResult GetGamesMissingPlayers();

        [HttpPost("Connect")]
        [Produces(typeof(GameDto))]
        Task<IActionResult> ConnectToGame(int gameId);

        [HttpPost("AddLine")]
        [Produces(typeof(GameDto))]
        Task<IActionResult> AddLine(LineCreationDto lineCreationDto);
        
        [HttpPost("CrossOut")]
        [Produces(typeof(GameDto))]
        Task<IActionResult> CrossOut(CrossoutCreationDto crossoutCreationDto);
    }
}