﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using DAL.Entities;
using DotsAndBoxes.Attributes;
using DotsAndBoxes.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DotsAndBoxes.Controllers
{
    [Authorize]
    [GameExceptionFilterAttribute()]
    [Route("api/[controller]")]
    public class GameController : Controller, IGameController
    {
        private readonly IGameService _gameService;
        
        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }
        
        [HttpGet("All")]
        [Produces(typeof(IEnumerable<GameDto>))]
        public IActionResult Get()
        {
            IEnumerable<Game> games = _gameService.Getll();
            List<GameDto> dtos = games.Select(g => g.ToDto()).ToList();
            return new ObjectResult(dtos);
        }

        [HttpGet("GetById")]
        [Produces(typeof(GameDto))]
        public IActionResult Get(int gameId)
        {
            Game game = _gameService.GetById(gameId);
            return new ObjectResult(game.ToDto());
        }

        [HttpPost("CreateGame")]
        public async Task<IActionResult> Post(int sizeX, int sizeY)
        {
            ThrowIfFieldSizeNotValid(sizeX, sizeY);
            Game game = await _gameService.CreateNewGame(User.Identity.Name, sizeX, sizeY);
            return new ObjectResult(game.ToDto());
        }

        [HttpGet("MissingPlayers")]
        [Produces(typeof(IEnumerable<GameDto>))]
        public IActionResult GetGamesMissingPlayers()
        {
            IEnumerable<Game> games = _gameService.GetGamesMissingPlayers();
            return new ObjectResult(games.Select(g => g.ToDto()).ToList());
        }

        [HttpPost("Connect")]
        [Produces(typeof(GameDto))]
        public async Task<IActionResult> ConnectToGame(int gameId)
        {
            Game joinedGame = await _gameService.ConnectToGame(User.Identity.Name, gameId);
            return new ObjectResult(joinedGame.ToDto());
        }
        
        [HttpPost("AddLine")]
        [Produces(typeof(GameDto))]
        public async Task<IActionResult> AddLine([FromBody] LineCreationDto lineCreationDto)
        {
            Game game = await _gameService.AddLine(User.Identity.Name, lineCreationDto.GameId, 
                lineCreationDto.X1,
                lineCreationDto.Y1,
                lineCreationDto.X2,
                lineCreationDto.Y2);
            
            return new ObjectResult(game.ToDto());
        }
        
        [HttpPost("CrossOut")]
        [Produces(typeof(GameDto))]
        public async Task<IActionResult> CrossOut([FromBody] CrossoutCreationDto crossoutCreationDto)
        {
            Game game = await _gameService.AddCrossOut(User.Identity.Name, 
                crossoutCreationDto.GameId, crossoutCreationDto.X, crossoutCreationDto.Y);
            
            return new ObjectResult(game.ToDto());
        }


        [SuppressMessage("ReSharper", "ParameterOnlyUsedForPreconditionCheck.Local")]
        private static void ThrowIfFieldSizeNotValid(int sizeX, int sizeY)
        {
            if (sizeX > 255 || sizeX < 3 || sizeY < 3 || sizeY > 255)
            {
                throw new InvalidDataException("Field size not valid. Minumim 3x3. Maximum 255x255");
            }
        }
    }
}
